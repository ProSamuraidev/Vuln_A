from bs4 import BeautifulSoup
import requests
import time
import datetime
import os
#Start
asset_name = ['nginx', 'redhat', 'mysql', 'suse', 'apache', 'tomcat', 'cisco', 'solarwinds', 'huawei', 'exchange', 'iis']
current_time = datetime.datetime.now()
day = current_time.day
week = day - 7
month = current_time.month
year = current_time.year
new_asset_list = []
user_asset_input_check = input("Asset Manualy: y/n: ")
#New List
cve_in_list = []
try:
    while user_asset_input_check == "y":
        asset_input = input("Enter Your Asset: ")
        new_asset_append = new_asset_list.append(asset_input)
except KeyboardInterrupt:
    pass

asset_use_check = input("Default Asset or Manual Asset? DA/MA: ")
if asset_use_check == "DA":
    asset_name = asset_name
elif asset_use_check == "MA":
    asset_name = new_asset_list
else:
    asset_name = asset_name
all_cves = open('all_cve.txt', 'w')
cve_in_list_file = open(f'all_cve_{day}.txt', 'w')
for asset in asset_name:
    nvd_url = f'https://nvd.nist.gov/vuln/search/results?form_type=Advanced&results_type=overview&query={asset}&search_type=all&mod_start_date={month}%2F{week}%2F{year}&mod_end_date={month}%2F{day}%2F{year}'
    get_nvd_url = requests.get(nvd_url).text
    soup = BeautifulSoup(get_nvd_url, 'lxml')
    asset_files = open(f'{asset}_{day}_{current_time.month}.txt', 'w')
    vuln_matching_record = soup.find('strong', attrs ={'data-testid': 'vuln-matching-records-count'} ).text
    detail_link = 1
    num_matching_record = int(vuln_matching_record)
    for detail_link in range(num_matching_record):
        nums = detail_link + 0
        vuln_cve = soup.find('a', attrs ={'data-testid': f'vuln-detail-link-{nums}'})
        vuln_cve_summary = soup.find('p', attrs ={'data-testid': f'vuln-summary-{nums}'})
        vuln_cve_cvss3 = soup.find('a', attrs = {'data-testid': f'vuln-cvss3-link-{nums}'})
        cve_in_list.append(vuln_cve.text)
        try:
            cve_with_asset = f'{asset}: {vuln_cve.text}: {vuln_cve_summary.text}: {vuln_cve_cvss3.text}' 
            cvewirte = asset_files.write(f'{cve_with_asset} \n \n')
            all_cves_writer = all_cves.write(f'{cve_with_asset} \n \n')
            cve_in_list_writer = cve_in_list_file.write(f'{cve_in_list}')
        except AttributeError:
            pass



    asset_files.close()
all_cves.close()
cve_in_list_file.close()

